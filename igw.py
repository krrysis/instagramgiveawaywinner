#@author: https://www.gitlab.com/krrysis @email: rockingdiv@gmail.com
#importing dependencies

from selenium import webdriver
from bs4 import BeautifulSoup as bs
import random

#ASCII ART
print("_ _  _ ____ ___ ____ ____ ____ ____ _  _") 
print("| |\ | [__   |  |__| | __ |__/ |__| |\/|") 
print("| | \| ___]  |  |  | |__] |  \ |  | |  |")
print(" ")
print("____ _ _  _ ____ ____ _ _ _ ____ _   _   ")
print("| __ | |  | |___ |__| | | | |__|  \_/    ")
print("|__] |  \/  |___ |  | |_|_| |  |   |     ")
print(" ")
print("_ _ _ _ _  _ _  _ ____ ____              ")
print("| | | | |\ | |\ | |___ |__/              ")
print("|_|_| | | \| | \| |___ |  \    ")
print("")
print("~by krrysis. https://www.gitlab.com/krrysis")
print("")

#get url of the post from the user
url=input("paste your post url: ")


#opening chrome via chromedriver

browser = webdriver.Chrome('chromedriver.exe')
#url=input("paste your post url")
browser.get(url)
#browser.get('https://www.instagram.com/p/B4rMd3-h3qX/') #this is the test link
Pagelength = browser.execute_script("window.scrollTo(0, document.body.scrollHeight);")

#parsing HTML

links=[]
source = browser.page_source
data=bs(source, 'html.parser')
body = data.find('body')
script = body.find('span')

#finding all anchor tags with class of instagram that has usernames that is: [FPmhX notranslate TlrDj]
hrefs = script.find_all("a", class_="FPmhX notranslate TlrDj")
for a in hrefs:
    #print (a['href'])
    links.append(a['href'])
    
#choosing the lucky guy instead and skipping the owner of the post whose name is always first in the list hence we start from 1
lucky=random.randint(1,len(links))
print(links[lucky])
print(len(links))